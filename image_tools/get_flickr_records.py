#!/usr/bin/python3

import requests
import csv
import string
import random
import time
from argparse import ArgumentParser


base_url = 'https://api.flickr.com/services/rest/'

def getRecCnt(params):
    resp = requests.get(base_url, params=params)
    res = resp.json()

    return int(res['photos']['total'])

def getLicenseTable(api_key):
    params = {
        'method': 'flickr.photos.licenses.getInfo', 'api_key': api_key,
        'format': 'json', 'nojsoncallback': 1
    }
    resp = requests.get(base_url, params=params)
    res = resp.json()

    l_table = {}
    for license in res['licenses']['license']:
        l_table[license['id']] = license['name']

    return l_table

def getRecords(base_params, writer, full_size):
    l_table = getLicenseTable(base_params['api_key'])

    params = base_params.copy()
    params['per_page'] = 200

    # Keep track of which photo IDs we've seen to ensure there are no
    # duplicates.
    photo_ids = set()

    # Initialize the stack of time intervals.
    end_ts = time.time()
    start_ts = time.mktime(time.strptime(
        'Jan 01 2004 00:00:00', '%b %d %Y %H:%M:%S'
    ))
    time_stack = [(start_ts, end_ts)]

    TF_STR = '%d %b %Y'

    # Remove time intervals from the stack until the stack is empty, splitting
    # intervals as needed to get the total number of records per interval below
    # 4,000, then downloading all records for each usable interval.
    while len(time_stack) > 0:
        start_ts, end_ts = time_stack.pop()

        params['min_upload_date'] = start_ts
        params['max_upload_date'] = end_ts

        rec_cnt = getRecCnt(params)

        if rec_cnt > 4000:
            print('Splitting interval with {0:,} records ({1} - {2})...'.format(
                rec_cnt, time.strftime(TF_STR, time.localtime(start_ts)),
                time.strftime(TF_STR, time.localtime(end_ts))
            ))
            mid_ts = ((end_ts - start_ts) / 2) + start_ts
            time_stack.append((mid_ts, end_ts))
            time_stack.append((start_ts, mid_ts))
        else:
            print('Getting {0:,} records for interval {1} - {2}...'.format(
                rec_cnt, time.strftime(TF_STR, time.localtime(start_ts)),
                time.strftime(TF_STR, time.localtime(end_ts))
            ))
            retrieveAllRecords(params, l_table, writer, full_size, photo_ids)

def retrieveAllRecords(base_params, l_table, writer, full_size, photo_ids):
    FNCHARS = string.digits + string.ascii_letters

    params = base_params.copy()
    if full_size:
        params['extras'] = 'license,url_o'
        URL_KEY = 'url_o'
        HEIGHT_KEY = 'height_o'
        WIDTH_KEY = 'width_o'
    else:
        params['extras'] = 'license,url_l'
        URL_KEY = 'url_l'
        HEIGHT_KEY = 'height_l'
        WIDTH_KEY = 'width_l'

    more_records = True
    record_cnt = photo_cnt = 0
    page = 1

    while more_records:
        params['page'] = page
        resp = requests.get(base_url, params=params)
        res = resp.json()

        page_cnt = int(res['photos']['pages'])
        #print(res['photos']['page'], page_cnt)

        if page >= page_cnt:
            more_records = False

        row_out = {}
        for rec in res['photos']['photo']:
            record_cnt += 1

            photo_id = rec['id']
            if photo_id in photo_ids:
                print('Repeat photo encountered: {0}'.format(photo_id))
            else:
                photo_ids.add(photo_id)

                if URL_KEY in rec:
                    photo_cnt += 1

                    img_fname = ''.join([
                        random.choice(FNCHARS) for cnt in range(16)
                    ]) + '.jpg'

                    row_out['img_id'] = photo_id
                    row_out['usr_id'] = rec['owner']
                    row_out['title'] = rec['title']
                    row_out['file_name'] = img_fname
                    row_out['img_url'] = rec[URL_KEY]
                    row_out['width'] = rec[WIDTH_KEY]
                    row_out['height'] = rec[HEIGHT_KEY]
                    row_out['license_code'] = rec['license']
                    row_out['license_text'] = l_table[rec['license']]
                    writer.writerow(row_out)

        print('  {0:,} records and {1:,} photos processed...'.format(
            record_cnt, photo_cnt
        ))
        page += 1

    print('done.')


argp = ArgumentParser(
    description='Downloads Flickr images for a given search text.'
)
argp.add_argument(
    '-k', '--api_key', type=str, required=True, help='A Flickr API key.'
)
argp.add_argument(
    '-o', '--output_file', type=str, required=False, default=None,
    help='An output CSV file name (default: "Flickr_images-SEARCH_TEXT.csv").'
)
argp.add_argument(
    '-c', '--counts_only', action='store_true',
    help='If this flag is set, only report record counts; do not actually '
    'download all records.'
)
argp.add_argument(
    '-f', '--full_size', action='store_true',
    help='If this flag is set, full-size images will be downloaded.  '
    'Otherwise, images will be limited to a maximum dimension of 1,024 pixels.'
)
argp.add_argument(
    'search_text', type=str, help='The search text to use.'
)

args = argp.parse_args()

base_params = {
    'method': 'flickr.photos.search', 'api_key': args.api_key,
    'text': args.search_text, 'format': 'json', 'nojsoncallback': 1
}

rec_cnt = getRecCnt(base_params)

if args.counts_only:
    msg = '\n{0:,} records available for "{1}".\n'.format(
        rec_cnt, args.search_text
    )
    print(msg)
else:
    ofpath = args.output_file
    if ofpath is None:
        ofpath = 'Flickr_images-' + args.search_text + '.csv'

    fout = open(ofpath, 'w')
    writer = csv.DictWriter(fout, [
        'img_id', 'usr_id', 'file_name', 'img_url', 'width', 'height',
        'license_code', 'license_text', 'title'
    ])
    writer.writeheader()

    getRecords(base_params, writer, args.full_size)

