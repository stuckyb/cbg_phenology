@echo off
setlocal

rem Expands all ZIP archives in the working directory.

for %%a in (*.zip) do (
    powershell Expand-Archive %%a .
)
exit /b