#!/usr/bin/python3

import csv
import sys


# Get the column names.
with open(sys.argv[1]) as fin:
    reader = csv.reader(fin)
    colnames = reader.__next__()

writer = csv.DictWriter(sys.stdout, colnames)
writer.writeheader()

for fname in sys.argv[1:]:
    with open(fname) as fin:
        reader = csv.DictReader(fin)

        for row in reader:
            if row['leaves'] != 'U':
                writer.writerow(row)

