#!/usr/bin/python3

# Extracts usable trait annotations from a CSV file of consensus annotations.

from argparse import ArgumentParser
import csv


argp = ArgumentParser(
   description='Extracts usable trait annotations from a CSV file of consensus '
   'annotations.'
)
argp.add_argument(
    '-n', '--none_val', type=str, required=False, default='N/A', help='The '
    'value to use for indicating an ambiguous consensus annotation (default: '
    '"N/A").'
)
argp.add_argument(
    '-i', '--ignore_vals', type=str, action='append', required=False,
    help='An annotation value to ignore when generating the output.  Can be '
    'used more than once to provide multiple annotation values to ignore.'
)
argp.add_argument(
    '-c', '--colname', type=str, required=True, help='The column of the input '
    'CSV file in which to search for usable annotations.'
)
argp.add_argument(
    '-o', '--output_file', type=str, required=True, help='The path of the '
    'output CSV file.'
)
argp.add_argument(
    'input_file', type=str, help='A consensus annotations file.'
)

args = argp.parse_args()

skip_vals = [args.none_val]
if args.ignore_vals is not None:
    skip_vals += args.ignore_vals

with open(args.input_file) as fin, open(args.output_file, 'w') as fout:
    reader = csv.DictReader(fin)
    writer = csv.DictWriter(fout, ['file', args.colname])
    writer.writeheader()

    row_out = {}

    for row in reader:
        if args.colname not in row:
            exit(
                '\nERROR: The column "{0}" was not found in {1}.\n'.format(
                    args.colname, args.input_file
                )
            )
        
        if row[args.colname] not in skip_vals:
            row_out['file'] = row['file']
            row_out[args.colname] = row[args.colname]
            writer.writerow(row_out)

